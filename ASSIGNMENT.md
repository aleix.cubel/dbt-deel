# Assignment 

This project was set up, developed, documented and tested in 3 real hours of work, including this file.

Here is a list of things I would add to the project if it was in a real world scenario:
- A `.github` directory with:
  - `pull_request_template.md`, to automate the PR template creation
  - `CODEOWNERS`, to automate PR review tagging to a set of GitHub users
  - a `/workflows` directory with CI/CD workflows, if GitHub Actions is being used
  - `dependabot.yml`, to track versions in `requirements.txt` and `github-actions`
- Encourge the use of other dbt packages:
  -  `dbt-osmosis` ([repo](https://github.com/z3z1ma/dbt-osmosis)), for automated schema YAML management. 
  -  `data-diff` ([repo](https://github.com/datafold/data-diff)), for diffing dev vs prod schema models when developing.
  -  `dbt2looker` ([repo](https://github.com/lightdash/dbt2looker)), for automated view creation in LookML.
- Other kind of materialisations, depending on the real data in the source. An idea would be to consider an `incremental` materialisation with a `merge` incremental strategy for both *chargebacks* and *acceptances*, in case fields such as `transaction_state` or `is_chargeback` were not prone to change.

## Part 1

1. **Preliminary data exploration**
  I've used the Snowflake UI for sense-checking the data, and Looker Studio to plot some charts
  of the staging models data.
  Here's the Looker Studio dashboard:
  ![Staging models EDA](imgs/eda_acceptance_report.png)
  **a. (top left)** Pie chart showing that we have the same amount of transaction for each country
  **b. (top mid)** Bar chart to see how many transactions were done providing the CVV
  **c. (top right)** Bar chart to see the transaction amount (in USD) by country
  **d. (bottom left)** Pivot table to see, by country, the transaction amount (in USD) for both accepted and declined operations
  **e. (bottom right)** Bar chart to see the amount of chargebacks


2. **Summary of your model architecture**
   - I separated directories between `staging` (by source), `intermediate` (by business unit), and `marts` (by business unit) layers. I also added a `utilities` directory, to gather models such as `dim_countries` - it would also be a good place to put a calendar table and an exchange rate model.
   - Each of the above has its own `schema.yml` and `docs.md`, where we test and document.
   - I added an intermediate model to join the acceptance_report and the chargeback_report source tables to improve the overall organisation of the data pipeline, provide modularity and reusability, and also to enforce a *layered architecture*, since this approach provides a clear separation of concerns and makes it easier to manage the complexity of the pipeline.


3. **Lineage graphs**
   `dbt docs generate && dbt docs serve` result:
    ![dbt DAG](imgs/final_dag.png)

4. **Tips around macros, data validation, and documentation**
   - **Testing and documentation** are key for maintaining a robust, efficient, reliable and healthy dbt project:
     - We want to *test our assumptions against the data* as much as we can, given that data is prone to change. When something breaks, we need to reassess whether our assumptions were correct and iterate if needed.
     - We want to make the lifes of our teammates (and ours!) easier, and we can do so by *documenting well all the fields and logic that we develop and own*.
   - In general, *as the project grows, testing and documentation become even more critical* for managing complexity and ensuring the long-term scalability of the project. Properly tested and documented code is more likely to be resilient in the face of changing data requirements and sources.
   - Macros are useful when we find chunks of code that are repetitive. To follow the DRY principle (Don't Repeat Youself), we can modularise pieces of code to be used more than once, elsewhere. I added an example in the repo, a macro called `reset_dev_env` (inside `macros/dev_env_maintenance`), that lets us clone all production models into our dev schema and delete any other remnant, making it much easier to start developing with a fresh version of prod by just one CLI command.
   - On the other hand, **linting (SQLFluff) and pre-commit checks** are pretty useful as well:
     - Linting helps the team follow the same coding style and conventions, making it easier to read the code.
     - Pre-commit checks are a way to consolidate other kind of conventions, such as enforcing model descriptions, naming of the models, and a minimum of tests per model (e.g. always test on uniquness and not_null), before we push stuff into PRs.


## Part 2

1. **What is the acceptance rate over time?**
  ```mysql
  select
    created_at::date,
    count(case when transaction_state = 'ACCEPTED' then order_id end)/ count(*)
    as acceptance_rate,
    -- we could also look at it from an amount perspective
    sum(case when transaction_state = 'ACCEPTED' then transaction_amount_usd end) / sum(transaction_amount_usd)
    as acceptance_rate_by_amount
  from analytics.analytics.fct_cc_transactions
  group by 1
  order by 1 asc;
  ```
  This query will show us, by date, the percentage (%) of accepted transactions in relation to the total amount of transactions in that specific date. If we wanted, we could change the date grain to week, month, quarter, etc. A chart will help us here:
  ![Acceptance Rate over time](imgs/acceptance_rate_time_series.png)

2. **List the countries where the amount of declined transactions went over $25M**
  ```mysql
  select
    country_name,
    sum(transaction_amount_usd) as total_amount_usd_declined
  from analytics.analytics.fct_cc_transactions
  where transaction_state = 'DECLINED'
  group by 1
  having total_amount_usd_declined > 25000000
  order by 2 asc;
  ```
  This query gives us the list of the three countries that got more than $25M declined transactions in the time period of the datasets provided.
  | country_name         | total_amount_usd_declined |
|----------------------|---------------------------|
| United States        | 25,125,674                |
| Canada               | 25,583,264                |
| United Arab Emirates | 26,335,151                |

3. **Which transactions are missing chargeback data?**
  ```mysql
  select *
  from analytics.analytics.fct_cc_transactions
  where not has_chargeback_info;
  ```
  As we computed this field in dbt, a simple `select from where` will return all the transactions that are missing chargeback information (that is, the transactions that do not appear in the chargeback_report source table), which appear to be none.