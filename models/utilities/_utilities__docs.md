{% docs country_name %}
Name of the country.
{% enddocs %}

{% docs country_group %}
The group assigned to this country within Deel's business logic. Either EFTA+UK, US+Canada, APAC or RoW.
{% enddocs %}