with seed as (
  select * from {{ ref('countries') }}
),

final as (
  select
    id,
    name as country_name,
    country_group,
    upper(iso2) as iso2,
    upper(iso3) as iso3,
    numeric_code as iso_numeric_code,
    phone_code,
    capital,
    currency,
    currency_name,
    tld as top_level_domain,
    region,
    subregion,
    latitude,
    longitude
  from seed
)

select * from final
