{% docs has_chargeback_info %}
Whether this transaction has information about chargebacks or not.
{% enddocs %}