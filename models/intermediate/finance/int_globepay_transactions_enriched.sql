with acceptance_report as (
  select * from {{ ref('stg_globepay__acceptance_report') }}
),

chargeback_report as (
  select * from {{ ref('stg_globepay__chargeback_report') }}
),

joined as (
  select
    acceptance_report.order_id,
    acceptance_report.globepay_order_id,

    acceptance_report.transaction_state,
    acceptance_report.is_cvv_provided,
    acceptance_report.transaction_amount_usd,
    acceptance_report.transaction_country,
    acceptance_report.transaction_currency,
    chargeback_report.is_chargeback,
    coalesce(chargeback_report.is_chargeback is not null, false) as has_chargeback_info,

    acceptance_report.created_at
  from acceptance_report
  left join chargeback_report
    on acceptance_report.globepay_order_id = chargeback_report.globepay_order_id
)

select * from joined
