with globepay_transactions_enriched as (
  select * from {{ ref('int_globepay_transactions_enriched') }}
),

countries as (
  select * from {{ ref('dim_countries') }}
),

joined as (
  select
    globepay_transactions_enriched.order_id,
    globepay_transactions_enriched.globepay_order_id,

    globepay_transactions_enriched.transaction_state,
    globepay_transactions_enriched.is_cvv_provided,
    globepay_transactions_enriched.transaction_amount_usd,
    globepay_transactions_enriched.transaction_country,
    globepay_transactions_enriched.transaction_currency,
    globepay_transactions_enriched.is_chargeback,
    coalesce(globepay_transactions_enriched.is_chargeback is not null, false)
    as has_chargeback_info,
    countries.country_name,
    countries.country_group,

    globepay_transactions_enriched.created_at
  from globepay_transactions_enriched
  left join countries
    on globepay_transactions_enriched.transaction_country = countries.iso2
)

select * from joined
