with seed as (
  select * from {{ ref('globepay_chargeback_report') }}
),

renamed as (
  select
    external_ref::varchar as globepay_order_id,
    status::boolean as has_status,
    source::varchar as transaction_source,
    chargeback::boolean as is_chargeback
  from seed
)

select * from renamed
