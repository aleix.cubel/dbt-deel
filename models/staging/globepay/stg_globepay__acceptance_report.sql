{% 
  set currencies = [
    'AUD', 'CAD', 'EUR', 'GBP', 'MXN', 'SGD', 'USD'
  ]
%}

with seed as (
  select * from {{ ref('globepay_acceptance_report') }}
),

renamed as (
  select
    ref::varchar as order_id,
    external_ref::varchar as globepay_order_id,

    status::boolean as has_status,
    source::varchar as transaction_source,
    state::varchar as transaction_state,
    cvv_provided::boolean as is_cvv_provided,
    amount::numeric as transaction_amount,
    country::varchar as transaction_country,
    currency::varchar as transaction_currency,
    parse_json(rates) as rates_json,

    case
    {%- for currency in currencies %}
      when transaction_currency = '{{ currency }}'
        then transaction_amount / rates_json:{{ currency }}::numeric
      {% if loop.last %}
      end as transaction_amount_usd,
      {%- endif -%}
    {%- endfor -%}
    date_time::timestamp as created_at
  from seed
)

select * from renamed
