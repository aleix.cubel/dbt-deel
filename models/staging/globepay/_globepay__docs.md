{% docs created_at %}
The timestamp of the transaction.
{% enddocs %}

{% docs transaction_state %}
The binary state of the transaction. Can be either Accepted or Declined.
{% enddocs %}

{% docs is_chargeback %}
Whether the transaction has been a chargeback or not.
{% enddocs %}

{% docs transaction_amount %}
The amount that has been charged from the card.
{% enddocs %}

{% docs transaction_amount_usd %}
The amount that has been charged from the card. Converted to USD using rates_json.
{% enddocs %}

{% docs transaction_currency %}
The three-character ISO currency code.
{% enddocs %}

{% docs transaction_country %}
The two-character ISO country code of the card.
{% enddocs %}

{% docs rates_json %}
A JSON object containing the exchange rate used. Funds are settled to you in USD.
{% enddocs %}

{% docs order_id %}
The ID of the Order.
{% enddocs %}

{% docs globepay_order_id %}
The ID of the order in the Globepay source.
{% enddocs %}

{% docs transaction_source %}
The source of the transaction. Should always be equal to GLOBALPAY.
{% enddocs %}

{% docs has_status %}
Whether the transaction has a status or not.
{% enddocs %}

{% docs is_cvv_provided %}
Whether the CVV was provided during the transaction or not.
{% enddocs %}