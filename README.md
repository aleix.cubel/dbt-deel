# Deel Data Warehouse - Snowflake

This [dbt project](https://docs.getdbt.com/) contains schema definitions and tests for the Deel Data Warehouse.

### Assignment

Please refer to `ASSIGNMENT.md` for all the home assignment-related questions.
### Prerequisites

Ask the Analytics team to create a Snowflake user with the needed role to develop.

### Getting Started

1. Clone this repo.

        git clone git@github.com:aleix-cd/dbt-deel.git

2. Install `dbt`.

        
        brew update
        brew tap dbt-labs/dbt
        brew install dbt-snowflake
        dbt deps
        

3. Copy `profiles.yml` to your `dbt` settings directory.
  
        
        mkdir ~/.dbt
        cd dbt-deel
        cp profiles.yml ~/.dbt/profiles.yml
        

4. Adapt the `profiles.yml` file with your information. A key-pair auth is required for security purposes

5. Run `dbt run-operation reset_dev_env --args '{from: analytics}'` to get a fresh copy of all production models.


### SQLFluff

`sqlfluff` is a linter for SQL. It currently does not run as part of our CircleCi build. However, you can still use it to lint your files locally
 Docs can be found [here](https://docs.sqlfluff.com/en/stable/).


1. Install `sqlfluff` in your virtual env.

        
        pip3 install sqlfluff-templater-dbt
        

2. Lint your files or directories.

        
        sqlfluff lint <path or file>
        

**Note**: To lint all files, run `sqlfluff lint` (don't specify path or file).

1. Fix your errors manually or autofix errors with built-in capability from `sqlfluff`. Double check anything that was autofixed!

        
        sqlfluff fix <path or file> --show-lint-violations
        sqlfluff fix <path or file> --rules <rule number> --show-lint-violations
        

### Pre-commit

`pre-commit` is a tool that allow us to check for rules as soon as we commit. All rules are specified in `.pre-commit-config.yaml`.

1. Install `pre-commit`.
        
        pre-commit install

2. If you want to disable pre-commit in a specific commit, add the `--no-verify` (or `-n` for short) flag into your commit command

        git commit -m -n "this is my commit message"

