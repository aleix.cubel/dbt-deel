{% macro clone_prod_to_target(from) %}

  -- Get all custom schemata for the given target
  {% set sql %}
    select
      replace(lower(schema_name), '{{ from }}') as schema
    from {{ target.database }}.information_schema.schemata
    where schema_name ilike '{{ from }}_%'
  {% endset %}

  {%- set schemata = dbt_utils.get_query_results_as_dict(sql) -%}

  {{ dbt_utils.log_info("With schema pattern '" ~ from ~ "_%', found custom schemata " ~ schemata ) }}

  -- Iterate through all schemata, incl the non custom one
  {% set sql -%}
    create schema if not exists {{ target.database }}.{{ target.schema }} clone {{ from }};
    {% for schema in schemata['SCHEMA'] -%}
      create schema if not exists {{ target.database }}.{{ target.schema }}{{ schema }} clone {{ from }}{{ schema }};
    {% endfor %}
  {%- endset %}

  {{ dbt_utils.log_info("Cloning schema(s) " ~ from ~ " into target schema(s).") }}

  {% do run_query(sql) %}

  {{ dbt_utils.log_info("Cloned schema(s) " ~ from ~ " into target schema(s).") }}

{% endmacro %}

{% macro destroy_current_env() %}

  {{ dbt_utils.log_info("Looking for " ~ target.schema ) }}

  -- Get all schemata for the given target
  {% set sql %}
    select
      lower(schema_name) as schema
    from {{ target.database }}.information_schema.schemata
    where schema_name ilike '{{ target.schema }}%'
  {% endset %}

  {%- set schemata = dbt_utils.get_query_results_as_dict(sql) -%}

  -- Check if target schema(s) exist, if not skip deletion to prevent SQL error
  {% if schemata['SCHEMA']|length > 0 %}
    {{ dbt_utils.log_info("Found schemata " ~ schemata ) }}

    -- Iterate through all schemata
    {% set sql -%}
      {% for schema in schemata['SCHEMA'] -%}
        drop schema if exists {{ target.database }}.{{ schema }} cascade;
      {% endfor %}
    {%- endset %}

    {{ dbt_utils.log_info("Dropping target schema(s).") }}

    {% do run_query(sql) %}

    {{ dbt_utils.log_info("Dropped target schema(s).") }}
  {% else %}
    {{ dbt_utils.log_info("Target schema(s) don't exist yet, nothing to do.") }}
  {% endif %}

{% endmacro %}

{% macro reset_dev_env(from) %}
{#-
This macro destroys your current development environment, and recreates it by cloning from prod.

To run it:
    $ dbt run-operation reset_dev_env --args '{from: analytics}'

-#}
    {% if target.name == 'dev' %}

    {{ destroy_current_env() }}

    {{ clone_prod_to_target(from) }}

    {% else %}

    {{ dbt_utils.log_info("No-op: your current target is " ~ target.name ~ ". This macro only works for a dev target.", info=True) }}

    {% endif %}

{% endmacro %}
